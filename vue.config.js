// vue.config.js
const path = require('path')
module.exports = {
  css: {
    loaderOptions: {
      less: {
        modifyVars: {
          // 直接覆盖变量
          // 'nav-bar-background-color': 'blue',
          /* 
            或者可以通过 less 文件覆盖（文件路径为绝对路径）
            __dirname 是node环境下的全局内置变量，指当下的项目文件
            __dirname 值为 D:\WorkSpace\project 
          */
          hack: `true; @import "${path.join(__dirname, 'src/styles/cover.less')}";`
        }
      }
    }
  }
}
