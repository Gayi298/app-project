import Vue from 'vue';
import VueRouter from 'vue-router';
import Login from '@/views/Login';
import Layout from '@/views/Layout';
import Home from '@/views/Home';
import User from '@/views/User';

Vue.use(VueRouter);

export default new VueRouter({
  routes: [
    {
      path: '/',
      // 首页重定向为登录界面
      redirect: '/login'
    },
    {
      path: '/login',
      component: Login
    },
    {
      path: '/layout',
      component: Layout,
      redirect: '/layout/home', //默认显示首页home
      children: [
        {
          path: 'home',
          component: Home
        },
        {
          path: 'user',
          component: User
        }
      ]
    }
  ]
})
