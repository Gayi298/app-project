// 统一进行网络请求管理【统一封装接口方法】
/*
    每个方法负责请求一个 url地址 ，逻辑页面导入接口方法就能发请求
    好处：请求 url地址 可在这个文件统一管理
    若后面产生跨域问题，前往vue.config.js配置代理服务器
*/
import request from './request.js';

// 登录接口
/* 
  Headers -- 请求头，axios内部，会自动携带请求参数
    Content-type:"application/json" 当请求头不是该内容时要写上 
  Body -- 请求体，需要页面绑定到组件数据里，传给接口
    用解构赋值，既是变量名，又是值
*/
export const loginAPI = ({ mobile, code }) => request({
  url: '/v1_0/authorizations',
  method: 'POST',
  data: {
    mobile,
    code
  }
})

// 获取所有频道
export const getAllChannelsAPI = () => request({
  url: '/v1_0/channels',
  method: 'GET'
})
