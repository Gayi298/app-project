// 基于axios封装网络请求
import axios from 'axios';

const request = axios.create({
  baseURL: 'http://geek.itheima.net/',
  timeout: 20000 // 请求20s无响应判定为超时
});



/*
  1.使用结构赋值，部分参数赋予默认值
  --> 传一个就能接收一个，不会出现方法改变，接收参数部分也要改动
  2.给axios封装一个自定义函数
*/
export default ({ url, method = 'GET', params = {}, data = {}, headers = {} }) => {
  return request({
    url,
    method,
    params,
    data,
    headers
  })
}

