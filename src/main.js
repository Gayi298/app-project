import Vue from 'vue';
import App from './App.vue';
import router from './router';
import store from './store';
// （移动端适配）引入flexible.js，设置根据根标签字体大小
import 'amfe-flexible';
import { 
  NavBar, 
  Form, 
  Field, 
  Button, 
  Toast, 
  Image as VanImage, 
  Tabbar,
  TabbarItem 
} from 'vant';

Vue.use(Tabbar);
Vue.use(TabbarItem);
Vue.use(VanImage);
Vue.use(Toast);
Vue.use(Button);
Vue.use(Form);
Vue.use(Field);
Vue.use(NavBar);

Vue.config.productionTip = false;

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
