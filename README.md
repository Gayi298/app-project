# project

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

# 目录结构
## src中
api -- 封装网络请求接口
assets -- 静态资源
components -- 静态组件
router -- 路由管理文件
store -- vuex管理文件
views -- 路由组件
style -- 样式文件、全局样式文件
utils -- 封装工具

## 总体文件夹的结构
├── node_modules # 安装的包
├── public	# 静态资源托管目录
│   ├── favicon.ico
│   └── index.html
└── src	# 源码
    ├── api	#请求接口封装模块
    	└── index.js #封装请求方法
    ├── assets	#资源目录
    ├── components	#组件目录
    ├── router	#路由模块
    	└── index.js #路由对象
    ├── store	#Vuex容器模块
    	└── index.js #store对象
    ├── styles #样式目录
    ├── utils  #工具模块目录
    ├── views  #视图组件目录
    ├── App.vue	#根组件
    └── main.js	#入口文件
├── .browserslistrc # 浏览器的约定
├── .editorconfig #对本项目要用到编辑器的约定
├── .eslintrc.js #eslint
├── .gitignore # git的忽略设置
├── babel.config.js	#babel配置文件
├── package-lock.json	#npm相关文件
├── package.json	#npm相关文件
└── README.md	#项目说明文件

# axios封装
## 封装内容
/*
  1.使用结构赋值，部分参数赋予默认值
  --> 传一个就能接收一个，不会出现方法改变，接收参数部分也要改动
  2.给axios封装一个自定义函数
*/
export default ({ url, method = 'GET', params = {}, data = {}, headers = {} }) => {
  return request({
    url,
    method,
    params,
    data,
    headers
  })
}

## 拦截器
【问题：加了拦截器请求有问题】
// 请求拦截器(发送请求前，请求拦截器能够检测到)
// request.interceptors.request.use((config) => {
//   // config:配置对象，对象里含有请求头 headers
//   return config;
// }, (error) => {
//   console.log('error',error)
//   return Promise.reject(error);
// });

// 响应拦截器
// request.interceptors.response.use((res) => {
//   // 成功的回调
//   return res.data;
// }, (error) => {
//   console.log('error',error)
//   return Promise.reject(error);
// })

/* 这种方法有弊端
    1、在哪里使用都需要写上5个配置项
    2、若将来需要更换请求方法，外部在传入时，可能依旧传入5个参数，
       但后续更改的方法$.ajax(没有params)不适用这些参数，它只有4个
    【里面的内容更换，但外面调用就对不上了。需要考虑到替换问题！！】
    因此，不能直接导出 axios函数。要给axios封装，才能向外导出。
*/
// export default axios
// axios({
//     url: '',
//     method: '',
//     params: {},
//     data: {},
//     headers: {}
// })


# 测试接口，并用try-catch捕获await错误(app.vue)
async created() {
    try {
      const res = await getAllChannelsAPI()
      console.log(res)
    }catch (err) {
      // 错误捕获 try-catch
      console.dir(err)
    }    
},

# 组件库的使用思路
1、明确目标，找到类似组件
2、引入、注册，并复制到相应的位置
3、读和删掉没用的部分
4、修改，成想要的样子

## 修改样式
--找到样式类名，自己写css覆盖【F12点击查看】(不推荐)
--看文档，“定制主题”，根据提示配置

# 登录界面构建
构成：logo、文字、用户名输入框、密码输入框、登录按键
--logo：图片
--文字：广东省行政执法（蓝字）
--用户名&密码文本框：文本框内有icon，未输入时有提示文字
--登录按钮：【校验功能】---------->提示都为vant的toast，文字提示
    -1.判断**是否有用户名**?
        否-->提示：请输入用户名
        有-->2.判断**是否有密码**？
                否-->提示：请输入密码
                是-->3.判断**账号密码是否正确**？
                        否-->提示：登录失败，请检查账号密码是否正确
                        是-->进入*首页*

