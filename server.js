// node服务器搭建
const express = require('express')
// const http = require('http')
// const fs = require('fs')
// const url = require('url')

// 创建应用对象
const app = express()

// 检查登录账号和密码
app.post('/login-check', (request, response) => {
  // 允许跨域
  response.setHeader('Access-Control-Allow-Origin', '*')
  // 响应头
  response.setHeader('Access-Control-Allow-Headers', '*')
  // 设置响应体
  response.send('hello')
})


/* // 创建web服务器对象
const server = http.createServer()
// 监听请求事件
server.on("request", (req, res) =>{
  res.setHeader('Content-Type', 'text/html;charset=utf-8')
  res.write("Hello world")
  res.end()
}) */

// 监听端口
server.listen(3000, () => {
  console.log("服务器启动成功, 3000端口监听中...")
})